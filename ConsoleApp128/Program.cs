﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp128
{
    abstract class Person
{
    public string Name { get; set; }
 
    public Person(string name)
    {
        Name = name;
    }
 
    public abstract void Display();
}
 
class Client : Person
{
    public int Sum { get; set; }    // сумма на счету
 
    public Client(string name, int sum)
        : base(name)
    {
        Sum = sum;
    }
    public override void Display()
    {
        Console.WriteLine($"{Name} имеет счет на сумму {Sum}");
    }
}
}
